#include <QApplication>
#include <QFile>
#include <QResizeEvent>
#include <QPushButton>
#include <QDebug>
#include <QStatusBar>

class Board : public QWidget
{
    int maxsize;
    QStatusBar *b;

public:
    Board (QWidget *parent = 0);


private:
    void resizeEvent (QResizeEvent *event);
    void drawTiles (int size);
};