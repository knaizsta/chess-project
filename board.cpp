#include "board.h"
#include "tile.h"


Board::Board(QWidget *parent) : QWidget(parent)
{
    QList<Tile*>tiles;
    maxsize = QGuiApplication::primaryScreen()->geometry().height() - 100;
    //anonymní políčka odmítaj spolupráci, čas vytvořit 64 neanonymních, konstruktory a přesouvání políček končí na řádku 202
    Tile * a8 = new Tile("white","BRook", 8, 1, this);
    a8 -> resize (50, 50);
    Tile * a7 = new Tile("black", "BPawn", 7, 1, this);
    a7 -> resize (50, 50);
    a7 -> move(0, 50);
    Tile * a6 = new Tile("white","none", 6, 1, this);
    a6 -> resize (50, 50);
    a6 -> move(0, 100);
    Tile * a5 = new Tile("black","none", 5, 1, this);
    a5 -> resize (50, 50);
    a5 -> move(0, 150);
    Tile * a4 = new Tile("white","none", 4, 1,this);
    a4 -> resize (50, 50);
    a4 -> move(0, 200);
    Tile * a3 = new Tile("black","none", 3, 1,this);
    a3 -> resize (50, 50);
    a3 -> move(0, 250);
    Tile * a2 = new Tile("white","WPawn", 2, 1, this);
    a2 -> resize (50, 50);
    a2 -> move(0,300);
    Tile * a1 = new Tile("black","WRook",1, 1, this);
    a1 -> resize (50,50);
    a1 -> move(0, 350);
    Tile * b8 = new Tile("black", "BKnight", 8, 2, this);
    b8 -> resize (50, 50);
    b8 -> move(50, 0);
    Tile * b7 = new Tile("white","BPawn", 7, 2, this);
    b7 -> resize (50,50);
    b7 -> move(50, 50);
    Tile * b6 = new Tile("black", "none", 6, 2, this);
    b6 -> resize (50,50);
    b6 -> move(50, 100);
    Tile * b5 = new Tile("white", "none", 5, 2, this);
    b5 -> resize (50,50);
    b5 -> move(50, 150);
    Tile * b4 = new Tile("black","none", 4, 2, this);
    b4 -> resize (50,50);
    b4 -> move(50, 200);
    Tile * b3 = new Tile("white","none", 3, 2, this);
    b3 -> resize (50,50);
    b3 -> move(50, 250);
    Tile * b2 = new Tile("black","WPawn", 2, 2, this);
    b2 -> resize (50,50);
    b2 -> move(50,300);
    Tile * b1 = new Tile("white","WKnight", 1, 2, this);
    b1 -> resize (50,50);
    b1 -> move(50, 350);
    Tile * h8 = new Tile("black", "BRook", 8, 8, this);
    h8 -> resize (50, 50);
    h8 -> move(350,0);
    Tile * h7 = new Tile("white", "BPawn", 7, 8, this);
    h7 -> resize (50,50);
    h7 -> move(350, 50);
    Tile * h6 = new Tile("black", "none", 6, 8, this);
    h6 -> resize (50,50);
    h6 -> move(350, 100);
    Tile * h5 = new Tile("white","none", 5, 8, this);
    h5 -> resize (50,50);
    h5 -> move(350, 150);
    Tile * h4 = new Tile("black","none", 4, 8, this);
    h4 -> resize (50,50);
    h4 -> move(350, 200);
    Tile * h3 = new Tile("white","none", 3, 8, this);
    h3 -> resize (50,50);
    h3 -> move(350, 250);
    Tile * h2 = new Tile("black","WPawn", 2, 8, this);
    h2 -> resize (50,50);
    h2 -> move(350,300);
    Tile * h1 = new Tile("white", "WRook", 1, 8, this);
    h1 -> resize (50,50);
    h1 -> move(350, 350);
    Tile * g8 = new Tile("white","BKnight", 8, 7, this);
    g8 -> resize (50, 50);
    g8 -> move(300, 0);
    Tile * g7 = new Tile("black","BPawn", 7, 7, this);
    g7 -> resize (50,50);
    g7 -> move(300, 50);
    Tile * g6 = new Tile("white","none", 6, 7, this);
    g6 -> resize (50,50);
    g6 -> move(300, 100);
    Tile * g5 = new Tile("black","none", 5, 7, this);
    g5 -> resize (50,50);
    g5 -> move(300, 150);
    Tile * g4 = new Tile("white","none", 4, 7, this);
    g4 -> resize (50,50);
    g4 -> move(300, 200);
    Tile * g3 = new Tile("black","none", 3, 7, this);
    g3 -> resize (50,50);
    g3 -> move(300, 250);
    Tile * g2 = new Tile("white","WPawn", 2, 7, this);
    g2 -> resize (50,50);
    g2 -> move(300,300);
    Tile * g1 = new Tile("black","WKnight", 1, 7, this);
    g1 -> resize (50,50);
    g1 -> move(300, 350);
    Tile * c8 = new Tile("white","BBishop", 8, 3, this);
    c8 -> resize (50, 50);
    c8 -> move(100,0);
    Tile * c7 = new Tile("black","BPawn", 7, 3, this);
    c7 -> resize (50,50);
    c7 -> move(100, 50);
    Tile * c6 = new Tile("white","none", 6, 3, this);
    c6 -> resize (50,50);
    c6 -> move(100, 100);
    Tile * c5 = new Tile("black","none", 5, 3, this);
    c5 -> resize (50,50);
    c5 -> move(100, 150);
    Tile * c4 = new Tile("white","none", 4, 3, this);
    c4 -> resize (50,50);
    c4 -> move(100, 200);
    Tile * c3 = new Tile("black","none", 3, 3, this);
    c3 -> resize (50,50);
    c3 -> move(100, 250);
    Tile * c2 = new Tile("white","WPawn", 2, 3, this);
    c2 -> resize (50,50);
    c2 -> move(100,300);
    Tile * c1 = new Tile("black","WBishop", 1, 3, this);
    c1 -> resize (50,50);
    c1 -> move(100, 350);
    Tile * f8 = new Tile("black","BBishop", 8, 6, this);
    f8 -> resize (50, 50);
    f8 -> move(250, 0);
    Tile * f7 = new Tile("white", "BPawn", 7, 6, this);
    f7 -> resize (50,50);
    f7 -> move(250, 50);
    Tile * f6 = new Tile("black","none", 6, 6, this);
    f6 -> resize (50,50);
    f6 -> move(250, 100);
    Tile * f5 = new Tile("white","none", 5, 6, this);
    f5 -> resize (50,50);
    f5 -> move(250, 150);
    Tile * f4 = new Tile("black","none", 4, 6, this);
    f4 -> resize (50,50);
    f4 -> move(250, 200);
    Tile * f3 = new Tile("white","none", 3, 6, this);
    f3 -> resize (50,50);
    f3 -> move(250, 250);
    Tile * f2 = new Tile("black","WPawn", 2, 6, this);
    f2 -> resize (50,50);
    f2 -> move(250,300);
    Tile * f1 = new Tile("white","WBishop", 1, 6, this);
    f1 -> resize (50,50);
    f1 -> move(250, 350);
    Tile * d8 = new Tile("black", "BQueen", 8, 4, this);
    d8 -> resize (50, 50);
    d8 -> move(150, 0);
    Tile * d7 = new Tile("white","BPawn", 7, 4, this);
    d7 -> resize (50,50);
    d7 -> move(150, 50);
    Tile * d6 = new Tile("black","none", 6, 4, this);
    d6 -> resize (50,50);
    d6 -> move(150, 100);
    Tile * d5 = new Tile("white","none", 5, 4, this);
    d5 -> resize (50,50);
    d5 -> move(150, 150);
    Tile * d4 = new Tile("black", "none", 4, 4, this);
    d4 -> resize (50,50);
    d4 -> move(150, 200);
    Tile * d3 = new Tile("white", "none", 3, 4, this);
    d3 -> resize (50,50);
    d3 -> move(150, 250);
    Tile * d2 = new Tile("black","WPawn", 2, 4, this);
    d2 -> resize (50,50);
    d2 -> move(150,300);
    Tile * d1 = new Tile("white","WQueen", 1, 4, this);
    d1 -> resize (50,50);
    d1 -> move(150, 350);
    Tile * e8 = new Tile("white","BKing", 8, 5, this);
    e8 -> castling = true;
    e8 -> resize (50, 50);
    e8 -> move(200, 0);
    Tile * e7 = new Tile("black","BPawn", 7, 5, this);
    e7 -> resize (50,50);
    e7 -> move(200, 50);
    Tile * e6 = new Tile("white","none", 6, 5, this);
    e6 -> resize (50,50);
    e6 -> move(200, 100);
    Tile * e5 = new Tile("black","none", 5, 5, this);
    e5 -> resize (50,50);
    e5 -> move(200, 150);
    Tile * e4 = new Tile("white","none", 4, 5, this);
    e4 -> resize (50,50);
    e4 -> move(200, 200);
    Tile * e3 = new Tile("black", "none", 3, 5, this);
    e3 -> resize (50,50);
    e3 -> move(200, 250);
    Tile * e2 = new Tile("white","WPawn", 2, 5, this);
    e2 -> resize (50,50);
    e2 -> move(200,300);
    Tile * e1 = new Tile("black", "WKing", 1, 5, this);
    e1 -> castling = true;
    e1 -> resize (50,50);
    e1 -> move(200, 350);

    tiles.push_back(a1);
    tiles.push_back(b1);
    tiles.push_back(c1);
    tiles.push_back(d1);
    tiles.push_back(e1);
    tiles.push_back(f1);
    tiles.push_back(g1);
    tiles.push_back(h1);
    tiles.push_back(a2);
    tiles.push_back(b2);
    tiles.push_back(c2);
    tiles.push_back(d2);
    tiles.push_back(e2);
    tiles.push_back(f2);
    tiles.push_back(g2);
    tiles.push_back(h2);
    tiles.push_back(a3);
    tiles.push_back(b3);
    tiles.push_back(c3);
    tiles.push_back(d3);
    tiles.push_back(e3);
    tiles.push_back(f3);
    tiles.push_back(g3);
    tiles.push_back(h3);
    tiles.push_back(a4);
    tiles.push_back(b4);
    tiles.push_back(c4);
    tiles.push_back(d4);
    tiles.push_back(e4);
    tiles.push_back(f4);
    tiles.push_back(g4);
    tiles.push_back(h4);
    tiles.push_back(a5);
    tiles.push_back(b5);
    tiles.push_back(c5);
    tiles.push_back(d5);
    tiles.push_back(e5);
    tiles.push_back(f5);
    tiles.push_back(g5);
    tiles.push_back(h5);
    tiles.push_back(a6);
    tiles.push_back(b6);
    tiles.push_back(c6);
    tiles.push_back(d6);
    tiles.push_back(e6);
    tiles.push_back(f6);
    tiles.push_back(g6);
    tiles.push_back(h6);
    tiles.push_back(a7);
    tiles.push_back(b7);
    tiles.push_back(c7);
    tiles.push_back(d7);
    tiles.push_back(e7);
    tiles.push_back(f7);
    tiles.push_back(g7);
    tiles.push_back(h7);
    tiles.push_back(a8);
    tiles.push_back(b8);
    tiles.push_back(c8);
    tiles.push_back(d8);
    tiles.push_back(e8);
    tiles.push_back(f8);
    tiles.push_back(g8);
    tiles.push_back(h8);

    for(int i = 0; i < 64; i++) //propojení tlačítek mezi sebou pro prohazování dílků
    {
        for(int j = 0; j < 64; j++)
        {
            QObject::connect(tiles.at(i), QPushButton::clicked, tiles.at(j), Tile::pieceMove);
        }
    }
}

void Board::resizeEvent (QResizeEvent *event)
{
    Q_UNUSED(event)
    // zjisteni nove velikosti okna
    // lze vyuzit i event->size()
    int width = this->geometry().width();
    int height = this->geometry().height();
    int size = (width > height) ? width : height;
    // maximalni velikost je dana vyskou displeje - viz konstruktor
    if (size > maxsize) size = maxsize;
    // nova velikost dlazdice
    int tsize = size/8;
    // zmena velikosti okna se zachovanim pomeru stran
    this->resize(8*tsize, 8*tsize);
    // vyhledani vsech potomku kontejneru typu QPushButton
    QList<QPushButton *> buttons = this->findChildren<QPushButton *>();
    // zmena velikosti a pozice vsech dlazdic
    for (int k = 0; k < buttons.size(); k++) 
    {
        int i = buttons.at(k)->x() / buttons.at(k)->width();
        int j = buttons.at(k)->y() / buttons.at(k)->height(); 
        buttons.at(k)->move (i*tsize, j*tsize);
        buttons.at(k)->resize(size/8, size/8);
    }
}

