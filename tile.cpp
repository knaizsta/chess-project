#include "tile.h"
#include <string.h>
#include <QStatusBar>
Tile::Tile(QString c, const char *p, int r, int f, QWidget *parent) : QPushButton(parent), color(c), active(false), rank(r), file(f), piece(p)
{
    updateStyle();
    setProperty(p, true); //zobrazování dílku probíhá přes stylesheet

    if (strcmp(p, "none") == 0) // rozhoduje o barvě dílku na políčku
        pieceColor = "none";
    else if (strcmp(p, "WPawn") == 0 || strcmp(p, "WRook") == 0 || strcmp(p, "WKnight") == 0 || strcmp(p, "WBishop") == 0 || strcmp(p, "WQueen") == 0 || strcmp(p, "WKing") == 0)
        pieceColor = "White";
    else
        pieceColor = "Black";

    QObject::connect(this, QPushButton::clicked, this, Tile::changeState);
}

void Tile::changeState()
{
    active = !active;
    updateStyle();
}

void Tile::updateStyle()
{
    setProperty("state", QVariant(QString("%1-%2").arg(color).arg(active)));
    // trik pro aktualizaci vlastnosti
    style()->unpolish(this);
    style()->polish(this);
}

void Tile::pieceMove() //funkce pro přesun dílků
{
    Tile *b = dynamic_cast<Tile *>(sender()); //políčko destinace
    if (this->active && b->active)            //podmínka zajišťující přesun pouze mezi vybranými políčky
    {
        if (strcmp(this->piece, "none") != 0 && strcmp(this->pieceColor, b->pieceColor) != 0) //tohle zamezuje chybě, kdy prázdné políčko sebere figurku nebo figurka sebere figurku vlastní barvy
        {
            if (strcmp(this->piece, "WPawn") == 0)
            {
                this->whitePawnMove(b);
            }
            else if (strcmp(this->piece, "BPawn") == 0)
            {
                this->blackPawnMove(b);
            }
            else if (strcmp(this->piece, "WKnight") == 0 || strcmp(this->piece, "BKnight") == 0)
            {
                this->horseyGoLshapes(b);
            }
            else if (strcmp(this->piece, "WRook") == 0 || strcmp(this->piece, "BRook") == 0)
            {
                this->rookMove(b);
            }
            else if (strcmp(this->piece, "WBishop") == 0 || strcmp(this->piece, "BBishop") == 0)
            {
                this->bishopMove(b);
            }
            else if (strcmp(this->piece, "WQueen") == 0 || strcmp(this->piece, "BQueen") == 0)
            {
                this->queenMove(b);
            }
            else if (strcmp(this->piece, "WKing") == 0 || strcmp(this->piece, "BKing") == 0)
            {
                this->kingMove(b);
            }
        }
        else
        {
            this->changeState();
            b->changeState();
        }
    }
}

void Tile::pieceExchange(Tile *b) //zařizuje přehození dílků mezi políčky
{
    this->setProperty(this->piece, false); //smaže figurky z políček
    b->setProperty(b->piece, false);

    b->piece = this->piece; // vymění figurky
    this->piece = "none";

    b->setProperty(b->piece, true); //zobrazí novou figurku na cílovém políčku

    b->pieceColor = this->pieceColor;
    this->pieceColor = "none";

    if (strcmp(b->piece, "WPawn") == 0 && b->rank == 8) //změní bílého pěšce v královnu, pokud se dostane na druhou stranu
    {
        b->piece = "WQueen";
        b->setProperty("WPawn", false);
        b->setProperty("WQueen", true);
    }

    if (strcmp(b->piece, "BPawn") == 0 && b->rank == 1) //změní pěšce černého v královnu, pokud se dostane na druhou stranu
    {
        b->piece = "BQueen";
        b->setProperty("BPawn", false);
        b->setProperty("BQueen", true);
    }

    this->changeState();
    b->changeState();
}

void Tile::whitePawnMove(Tile *b) //pohyb bílého pěšce
{
    if (this->rank == 2)
    {
        if (b->rank <= this->rank + 2 && this->file == b->file && strcmp(b->piece, "none") == 0 && b->rank > 1) // pohyb o jedno nebo dvě políčka vpřed pokud jsou pěšáci stále na původní pozici
        {
            this->pieceExchange(b);
        }
        else if (b->rank == this->rank + 1 && (b->file == this->file + 1 || b->file == this->file - 1) && strcmp(b->piece, "none") != 0) //braní šikmo, pokud políčko není prázdné
        {
            this->pieceExchange(b);
        }
        else
        {
            this->changeState();
            b->changeState();
        }
    }
    else if (this->file == b->file && b->rank == this->rank + 1 && strcmp(b->piece, "none") == 0) //pohyb vpřed, pokud je políčko před pěšcem prázdné
        this->pieceExchange(b);
    else if (b->rank == this->rank + 1 && (b->file == this->file + 1 || b->file == this->file - 1) && strcmp(b->piece, "none") != 0)
        this->pieceExchange(b);
    else
    {
        this->changeState();
        b->changeState();
    }
}

void Tile::blackPawnMove(Tile *b) //pohyb černého pěšce
{
    if (this->rank == 7)
    {
        if (b->rank >= this->rank - 2 && this->file == b->file && strcmp(b->piece, "none") == 0 && b->rank < 8) // pohyb o jedno nebo dvě políčka vpřed pokud jsou pěšáci stále na původní pozici
        {
            this->pieceExchange(b);
        }
        else if (b->rank == this->rank - 1 && (b->file == this->file + 1 || b->file == this->file - 1) && strcmp(b->piece, "none") != 0) //braní šikmo, pokud políčko není prázdné
        {
            this->pieceExchange(b);
        }
        else
        {
            this->changeState();
            b->changeState();
        }
    }
    else if (this->file == b->file && b->rank == this->rank - 1 && strcmp(b->piece, "none") == 0) //pohyb vpřed, pokud je políčko před pěšcem prázdné
        this->pieceExchange(b);
    else if (b->rank == this->rank - 1 && (b->file == this->file + 1 || b->file == this->file - 1) && strcmp(b->piece, "none") != 0)
        this->pieceExchange(b);
    else
    {
        this->changeState();
        b->changeState();
    }
}

void Tile::horseyGoLshapes(Tile *b) //pohyb jezdců
{
    if (((b->rank == this->rank - 1 || b->rank == this->rank + 1) && (b->file == this->file - 2 || b->file == this->file + 2)) || ((b->rank == this->rank - 2 || b->rank == this->rank + 2) && (b->file == this->file - 1 || b->file == this->file + 1))) // absurdní podmínka obsahující všech 8 políček pro pohyb jezdce
        this->pieceExchange(b);
    else
    {
        this->changeState();
        b->changeState();
    }
}

void Tile::rookMove(Tile *b) //pohyb věží
{
    if (this->rank == b->rank || this->file == b->file)
        this->pieceExchange(b);
    else
    {
        this->changeState();
        b->changeState();
    }
}

void Tile::bishopMove(Tile *b) //pohyb střelcem
{
    for (int i = 1; i < 9; i++)
    {
        if ((this->rank == b->rank + i || this->rank == b->rank - i) && (this->file == b->file + i || this->file == b->file - i)) //kontrola diagonálního pohybu
        {
            this->pieceExchange(b);
            break;
        }
        else if (i == 8)
        {
            this->changeState();
            b->changeState();
        }
    }
}

void Tile::queenMove(Tile *b) //pohyb královnou
{
    if (this->rank == b->rank || this->file == b->file)
        this->pieceExchange(b);
    else
    {
        for (int i = 1; i < 9; i++)
        {
            if ((this->rank == b->rank + i || this->rank == b->rank - i) && (this->file == b->file + i || this->file == b->file - i))
            {
                this->pieceExchange(b);
                break;
            }
            else if (i == 8)
            {
                this->changeState();
                b->changeState();
            }
        }
    }
}

void Tile::kingMove(Tile *b) //pohyb králem
{
    if (this->castling) //pokud má nárok na rošádu, může se král pohnout o dvě políčka do strany, to usnadní rošádu
    {
        if ((this->rank == b->rank + 1 || this->rank == b->rank - 1 || this->rank == b->rank) && ((this->file <= b->file + 2 && this->file >= b->file - 2) || this->file == b->file))
        {
            this->pieceExchange(b);
            this->castling = false;
        }
        else
        {
            this->changeState();
            b->changeState();
        }
    }
    else if ((this->rank == b->rank + 1 || this->rank == b->rank - 1 || this->rank == b->rank) && (this->file == b->file + 1 || this->file == b->file - 1 || this->file == b->file))
        this->pieceExchange(b);
    else
    {
        this->changeState();
        b->changeState();
    }
}