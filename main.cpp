#include "board.h"

int main(int argc, char * argv[])
{
    QApplication app(argc, argv);

    Q_INIT_RESOURCE(chessboard);
    QFile stylesheet(":/resources/styles/chessboard.qss");

    if (!stylesheet.open(QIODevice::ReadOnly)) {
        qWarning("Unable to open :/res/chessboard.qss");
    }

    app.setStyleSheet(stylesheet.readAll());

    Board b;
    b.setWindowTitle("Chess");
    b.show();
    
    return app.exec();
}