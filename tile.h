

#include <QPushButton>
#include <QVariant>
#include <QStyle>

class Tile : public QPushButton
{
    Q_OBJECT
    
    QString color; 
    bool active;
    const char * pieceColor; // dílek a jeho barva je uložená pomocí stringu
    int rank, file; //souřadnice políčka, rank je řádek, file sloupec
    const char * piece;
    
    void updateStyle ();

public:
    bool castling; //zda má král nárok na rošádu
    Tile (QString c, const char * p, int r, int f, QWidget *parent = 0);
    void pieceExchange(Tile* b);
    void whitePawnMove(Tile* b);
    void blackPawnMove(Tile* b);
    void horseyGoLshapes(Tile* b);
    void bishopMove(Tile* b);
    void rookMove(Tile* b);
    void queenMove(Tile* b);
    void kingMove(Tile* b);


public slots:
    void changeState ();
    void pieceMove();
};

